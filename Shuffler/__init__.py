# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging


import logging

def main(pairs: str) -> str:
    shuffle_result = {}
    for pair in pairs:
        if pair[0] in shuffle_result.keys():
            val = shuffle_result[pair[0]]
            val.append(pair)
            print(val, type(val))
            shuffle_result.update({pair[0]:val})
        else:
            shuffle_result[pair[0]] = [pair]
    return shuffle_result
