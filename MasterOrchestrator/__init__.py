# This function is not intended to be invoked directly. Instead it will be
# triggered by an HTTP starter function.
# Before running this sample, please:
# - create a Durable activity function (default name is "Hello")
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import json

import azure.functions as func
import azure.durable_functions as df


def orchestrator_function(context: df.DurableOrchestrationContext):
    root_directory: str = context.get_input()
    try:
        # dataset = yield context.call_activity('GetInputData', root_directory)
        dataset = '''With tuppence for paper and strings
                    You can have your own set of wings
                    With your feet on the ground
                    You're a bird in flight
                    With your fist holding tight
                    To the string of your kite'''
        after_mapping = yield context.call_activity('Mapper', dataset)
        after_shuffling = yield context.call_activity('Shuffler', after_mapping)
        result = yield context.call_activity('Shuffler', after_shuffling)
        return result
    except Exception as exc:
        return f'Somethin went wrong with {exc}'


main = df.Orchestrator.create(orchestrator_function)
