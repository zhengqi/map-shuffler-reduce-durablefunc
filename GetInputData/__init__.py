# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import os, uuid
from azure.identity import DefaultAzureCredential
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient

def main(root_directory: str) -> str:
    try:
        print("Azure Blob Storage Python quickstart sample")
        # Download the blob to a local file
        # Add 'DOWNLOAD' before the .txt extension so you can see both files in the data directory
        # download_file_path = os.path.join(local_path, str.replace(local_file_name ,'.txt', 'DOWNLOAD.txt'))
        # container_client = blob_service_client.get_container_client(container= container_name) 
        # print("\nDownloading blob to \n\t" + download_file_path)

        # with open(file=download_file_path, mode="wb") as download_file:
        # download_file.write(container_client.download_blob(blob.name).readall())
        # return 
    except Exception as ex:
        print('Exception:')
        print(ex)
    
    
